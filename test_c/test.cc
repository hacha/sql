#include <iostream>
#include <string>
#include <mysql/mysql.h>

//const std::string host = "127.0.0.1";
const std::string host = "localhost";
const std::string user = "hx";
const std::string passwd = "123456";
const std::string db = "scott";
const unsigned int port = 3306;

int main()
{
    // std::cout << "mysql client version :" << mysql_get_client_info() << std::endl;
    
    MYSQL *my = mysql_init(nullptr);
    if(nullptr == my)
    {
        std::cerr << "init MySQL error" << std::endl;
        return 1;
    }

    if(mysql_real_connect(my,host.c_str(),user.c_str(),passwd.c_str(),db.c_str(),port,nullptr,0) == nullptr)
    {
        std::cerr << "connect MySQL error" << std::endl;
        return 2;
    }
    // 解决乱码问题
    mysql_set_character_set(my, "utf8");


    //std:: string sql = "update stu set name='HXHX' where id = 1 "; 
    //std::string sql = "insert stu(id,name) values(9,'洪兴')";
    std::string sql = "select * from stu";
    int n = mysql_query(my,sql.c_str());
    if(n == 0) std::cout << sql << "success" << std::endl;
    else 
    {   
        std::cout << sql << "failed" << std::endl;
        return 3;
    } 

    MYSQL_RES *res = mysql_store_result(my);
    if(nullptr == res)
    {
        std::cerr << "mysql_store_result error" << std::endl;
        return 4;
    }

    // 获取对应的行,列
    my_ulonglong rows = mysql_num_rows(res);
    my_ulonglong fields = mysql_num_fields(res);

    std::cout << "行：" << rows << std::endl;
    std::cout << "列：" << fields << std::endl;

    // 属性
    MYSQL_FIELD *fields_array = mysql_fetch_fields(res);
    for(int i = 0;i<fields;i++)
    {
        std::cout << fields_array[i].name << "\t";
    }
    std::cout << "\n";

    // 内容
    for(int i = 0; i < rows; i++)
    {
        MYSQL_ROW row  = mysql_fetch_row(res);
        for(int j = 0; j < fields; j++)
        {
            std::cout << row[j] << "\t";
        }
        std::cout << "\n";
    }

    // 其他属性
    std::cout << fields_array[0].db <<" " << fields_array[0].table << std::endl; 


    // std::string sql;
    // while(true)
    // {
    //     std::cout << "MySQL>>>";
    //     // getline 读取一行sql到cin当中
    //     if(!std::getline(std::cin,sql) || sql == "quit")
    //     {
    //         std::cout << "ByeBye~" << std::endl;
    //         break;
    //     }
    //     int n = mysql_query(my,sql.c_str());
    //     // 如果n == 0 那么就获取到sql中的语句
    //     if(n == 0)
    //     {
    //         std::cout << sql << "success: " << n << std::endl;
    //     }
    //     else
    //     {
    //         std::cerr << sql << "failed: " << n << std::endl;
    //     }
    // }


    // std::cout << "connect success" << std::endl;
    
    // free 释放内存空间，防止内存泄漏 
    mysql_free_result(res);
    mysql_close(my);
    return 0;
}