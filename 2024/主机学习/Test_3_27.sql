/*
-- 数据插入emp表当中
insert into dbo.emp (empno,ename,job,mgr,hiredate,sal,comm,deptno)values (007369,'SMITH', 'CLERK',7902,'1980-12-17 00:00:00',800,NULL,20);



-- 数据插入dept表当中
insert into dbo.dept values (10,'ACCOUNTING','NEW YORK' );
insert into dbo.dept values (20,'RESEARCH','DALLAS');
insert into dbo.dept values (30,'SALES','CHICAGO');
insert into dbo.dept values (40,'OPERATIONS','BOSTON');
*/


/* 
删除重复行数据

WITH CTE AS (
    SELECT *, ROW_NUMBER() OVER (PARTITION BY empno,ename,job,mgr,hiredate,sal,comm,deptno ORDER BY (SELECT NULL)) AS rn
    FROM dbo.emp
)
DELETE FROM CTE WHERE rn > 1;

*/
-- select * from dbo.dept;

-- select * from dbo.emp;

-- select * from dbo.salgrade;

/*
SELECT *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'emp'

*/


-- select * from emp where ename  ='SMITH';
-- select * from emp where deptno = (select deptno from emp where ename  ='SMITH');

-- select AVG(sal) from emp;
select * from emp where sal > (select AVG(sal) from emp);

