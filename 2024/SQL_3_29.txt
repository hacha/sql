-- 查看
mysql> SELECT @@global.tx_isolation; --查看全局隔级别
+-----------------------+
| @@global.tx_isolation |
+-----------------------+
|       REPEATABLE-READ |
+-----------------------+
1 row in set, 1 warning (0.00 sec)

mysql> SELECT @@session.tx_isolation; --查看会话(当前)全局隔级别
+------------------------+
| @@session.tx_isolation |
+------------------------+
|        REPEATABLE-READ |
+------------------------+
1 row in set, 1 warning (0.00 sec)

mysql> SELECT @@tx_isolation; --默认同上
+-----------------+
|  @@tx_isolation |
+-----------------+
| REPEATABLE-READ |
+-----------------+
1 row in set, 1 warning (0.00 sec)


-- 设置
-- 设置当前会话 or 全局隔离级别语法
SET [SESSION | GLOBAL] TRANSACTION ISOLATION LEVEL
{READ UNCOMMITTED | READ COMMITTED | REPEATABLE READ | SERIALIZABLE}
-- 设置当前会话隔离性，另起一个会话，看不多，只影响当前会话
mysql> set session transaction isolation level serializable; -- 串行化
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT @@global.tx_isolation; --全局隔离性还是RR
+-----------------------+
| @@global.tx_isolation |
+-----------------------+
|       REPEATABLE-READ |
+-----------------------+
1 row in set, 1 warning (0.00 sec)

mysql> SELECT @@session.tx_isolation; --会话隔离性成为串行化
+------------------------+
| @@session.tx_isolation |
+------------------------+
|           SERIALIZABLE |
+------------------------+
1 row in set, 1 warning (0.00 sec)

mysql> SELECT @@tx_isolation; --同上
+----------------+
| @@tx_isolation |
+----------------+
|   SERIALIZABLE |
+----------------+
1 row in set, 1 warning (0.00 sec)

--设置全局隔离性，另起一个会话，会被影响
mysql> set global transaction isolation level READ UNCOMMITTED;
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT @@global.tx_isolation;
+-----------------------+
| @@global.tx_isolation |
+-----------------------+
|      READ-UNCOMMITTED |
+-----------------------+
1 row in set, 1 warning (0.00 sec)

mysql> SELECT @@session.tx_isolation;
+------------------------+
| @@session.tx_isolation |
+------------------------+
|       READ-UNCOMMITTED |
+------------------------+
1 row in set, 1 warning (0.00 sec)

mysql> SELECT @@tx_isolation;
+------------------+
|   @@tx_isolation |
+------------------+
| READ-UNCOMMITTED |
+------------------+
1 row in set, 1 warning (0.00 sec)

-- 注意，如果没有现象，关闭mysql客户端，重新连接。

--几乎没有加锁，虽然效率高，但是问题太多，严重不建议采用
--终端A
-- 设置隔离级别为 读未提交
mysql> set global transaction isolation level read uncommitted;
Query OK, 0 rows affected (0.00 sec)

--重启客户端
mysql> select @@tx_isolation;
+------------------+
|   @@tx_isolation |
+------------------+
| READ-UNCOMMITTED |
+------------------+
1 row in set, 1 warning (0.00 sec)

mysql> select * from account;
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |   100.00 |
|  2 |    李四 | 10000.00 |
+----+--------+----------+
2 rows in set (0.00 sec)

mysql> begin; --开启事务
Query OK, 0 rows affected (0.00 sec)

mysql> update account set blance=123.0 where id=1; --更新指定行
Query OK, 1 row affected (0.05 sec)
Rows matched: 1 Changed: 1 Warnings: 0
--没有commit哦！！！

--终端B
mysql> begin;
Query OK, 0 rows affected (0.00 sec)

mysql> select * from account;
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |   123.00 | --读到终端A更新但是未commit的数据[insert，delete同样]
|  2 |    李四 | 10000.00 |
+----+--------+----------+
2 rows in set (0.00 sec)
-- 一个事务在执行中，读到另一个执行中事务的更新(或其他操作)但是未commit的数据，这种现象叫做脏读(dirty read)

-- 终端A
mysql> set global transaction isolation level read committed;
Query OK, 0 rows affected (0.00 sec)

--重启客户端
mysql> select * from account; --查看当前数据
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |   123.00 | 
|  2 |    李四 | 10000.00 |
+----+--------+----------+
2 rows in set (0.00 sec)

mysql> begin; --手动开启事务，同步的开始终端B事务
Query OK, 0 rows affected (0.00 sec)

mysql> update account set blance=321.0 where id=1; --更新张三数据
Query OK, 1 row affected (0.00 sec)
Rows matched: 1 Changed: 1 Warnings: 0

--切换终端到终端B，查看数据。

mysql> commit; --commit提交！
Query OK, 0 rows affected (0.01 sec)

--切换终端到终端B，再次查看数据。

--终端B
mysql> begin; --手动开启事务，和终端A一前一后
Query OK, 0 rows affected (0.00 sec)

mysql> select * from account; --终端A commit之前，查看不到
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |   123.00 | --老的值
|  2 |    李四 | 10000.00 |
+----+--------+----------+
2 rows in set (0.00 sec)

--终端A commit之后，看到了！
--but，此时还在当前事务中，并未commit，那么就造成了，同一个事务内，同样的读取，在不同的时间段
(依旧还在事务操作中！)，读取到了不同的值，这种现象叫做不可重复读(non reapeatable read)！！
（这个是问题吗？？）
mysql> select *from account;
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |   321.00 | --新的值
|  2 |    李四 | 10000.00 |
+----+--------+----------+
2 rows in set (0.00 sec)

--终端A
mysql> set global transaction isolation level repeatable read; --设置全局隔离级别RR
Query OK, 0 rows affected (0.01 sec)

--关闭终端重启

mysql> select @@tx_isolation;
+-----------------+
|  @@tx_isolation |
+-----------------+
| REPEATABLE-READ | --隔离级别RR
+-----------------+
1 row in set, 1 warning (0.00 sec)

mysql> select *from account; --查看当前数据
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |   321.00 | 
|  2 |    李四 | 10000.00 |
+----+--------+----------+
2 rows in set (0.00 sec)

mysql> begin; --开启事务，同步的，终端B也开始事务
Query OK, 0 rows affected (0.00 sec)

mysql> update account set blance=4321.0 where id=1; --更新数据
Query OK, 1 row affected (0.00 sec)
Rows matched: 1 Changed: 1 Warnings: 0

--切换到终端B，查看另一个事务是否能看到

--终端B
mysql> begin;
Query OK, 0 rows affected (0.00 sec)

mysql> select * from account; --终端A中事务 commit之前，查看当前表中数据，数据未更新
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |   321.00 | 
|  2 |    李四 | 10000.00 |
+----+--------+----------+
2 rows in set (0.00 sec)

mysql> select * from account; --终端A中事务 commit 之后，查看当前表中数据，数据未更新
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |  321.00 | 
|  2 |    李四 | 10000.00 |
+----+--------+----------+
2 rows in set (0.00 sec)

--可以看到，在终端B中，事务无论什么时候进行查找，看到的结果都是一致的，这叫做可重复读！

mysql> commit; --结束事务
Query OK, 0 rows affected (0.00 sec)

mysql> select * from account; --再次查看，看到最新的更新数据
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |  4321.00 | 
|  2 |    李四 | 10000.00 |
+----+--------+----------+
2 rows in set (0.00 sec)

--如果将上面的终端A中的update操作，改成insert操作，会有什么问题？？
--终端A

mysql> select *from account;
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |   321.00 | 
|  2 |    李四 | 10000.00 |
+----+--------+----------+
2 rows in set (0.00 sec)

mysql> begin; --开启事务，终端B同步开启
Query OK, 0 rows affected (0.00 sec)

mysql> insert into account (id,name,blance) values(3, '王五', 5432.0);
Query OK, 1 row affected (0.00 sec)

--切换到终端B，查看另一个事务是否能看到

mysql> commit; --提交事务
Query OK, 0 rows affected (0.00 sec)

--切换终端到终端B，查看数据。
mysql> select * from account;
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |  4321.00 | 
|  2 |    李四 | 10000.00 |
|  3 |    王五 |  5432.00 |
+----+--------+----------+
3 rows in set (0.00 sec)

--终端B
mysql> begin; --开启事务
Query OK, 0 rows affected (0.00 sec)

mysql> select * from account; --终端A commit前 查看
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |   321.00 | 
|  2 |    李四 | 10000.00 |
+----+--------+----------+
2 rows in set (0.00 sec)
mysql> select * from account; --终端A commit后 查看
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |   321.00 | 
|  2 |    李四 | 10000.00 |
+----+--------+----------+
2 rows in set (0.00 sec)



mysql> commit; --结束事务
Query OK, 0 rows affected (0.00 sec)

mysql> select * from account; --看到更新
+----+--------+----------+
| id |   name |   blance |
+----+--------+----------+
|  1 |    张三 |  4321.00 | 
|  2 |    李四 | 10000.00 |
|  3 |    王五 |  5432.00 |
+----+--------+----------+
3 rows in set (0.00 sec)







