import pyodbc
import pandas as pd

# 假设data是cursor.fetchall()返回的结果，它是一个包含元组的列表
data = [(1, 700, 1200), (2, 1201, 1400), (3, 1401, 2000), (4, 2001, 3000), (5, 3001, 9999)]
print(type(data))
print(data)
# 获取列名
columns = ['grade', 'losal', 'hisal']  # 确保这些列名与您的表中的列名相匹配
print(type(columns))
print(columns)

# 将结果转换为DataFrame
df = pd.DataFrame(list(data), columns=columns)
print(df)

